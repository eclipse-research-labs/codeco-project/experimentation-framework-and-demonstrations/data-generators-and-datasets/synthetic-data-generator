# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Panagiotis Karamolegkos (UPRC) - Author

FROM python:3.10

WORKDIR /app

# Get the files to be used in the container
# COPY acm-controller/acm-controller.py /app
# COPY acm-controller/acm-crd.yaml /app
# COPY generation/traffic_generator.py /app
# COPY generation/test_gen.py /app
# COPY generation/requirements.txt /app

# Copy the files needed for the controller
COPY . .

# Upgrade pip
RUN pip install --no-cache-dir --upgrade pip

RUN pip install --no-cache-dir -r requirements.txt

# This will be the default value for the environment variable
ENV DATA_PATH="/app/config.json"

CMD ["python", "-u", "acm-controller.py"]
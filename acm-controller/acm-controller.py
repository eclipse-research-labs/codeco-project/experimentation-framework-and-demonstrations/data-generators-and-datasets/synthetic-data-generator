# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Efterpi Paraskevoulakou (UPRC) - Author

# Import necessary libraries
import os
import random
import time as t
import yaml
from kubernetes import client, config

# Load the Kubernetes configuration from the default location
config.load_incluster_config()
v1 = client.CustomObjectsApi()

# Define constants
LIST_OF_NODES = os.getenv("LIST_OF_NODES", "node_1,node_2,node_3")
CR_NAMESPACE = os.getenv("ACM_CR_NAMESPACE", "default")
CRD_FILE_PATH = "acm-crd.yaml"
CRD_GROUP = "codeco.he-codeco.eu"
CRD_VERSION = "v1alpha1"
CRD_PLURAL = "codecoapps"
CR_NAME = "acm-sample"



def get_spec():
    # Define a function to generate the spec and status
    avg_service_cpu = random.randint(50, 90)
    avg_service_memory = random.randint(2, 6)
    avg_service_failure = random.choice([0, 1])
    avg_node_cpu = random.randint(30, 80)
    avg_node_energy = random.randint(2, 6)
    avg_node_failure = random.choice([0, 1])

    spec_body = {
        "spec": {
            "appEnergyLimit": "20",
            "appFailureTolerance": "",
            "appName": "acm-swm-app",
            "codecoapp-msspec": [
                {
                    "nwbandwidth": "1.2",
                    "nwlatency": "3",
                    "podspec": {
                        "containers": [
                            {
                                "image": "quay.io/skupper/hello-world-backend:latest",
                                "name": "skupper-backend",
                                "ports": [
                                    {
                                        "containerPort": 8080,
                                        "name": "skupper-backend",
                                        "protocol": "TCP"
                                    }
                                ],
                                "resources": {
                                    "limits": {
                                        "cpu": "2",
                                        "memory": "4Gi"
                                    }
                                }
                            }
                        ]
                    },
                    "serviceChannels": [
                        {
                            "advancedChannelSettings": {
                                "bandwidth": "5",
                                "frameSize": "100",
                                "maxDelay": "1",
                                "minBandwidth": "5",
                                "sendInterval": "10"
                            },
                            "channelName": "frontend",
                            "otherService": {
                                "appName": "acm-swm-app",
                                "port": 9090,
                                "serviceName": "front-end"
                            }
                        }
                    ],
                    "serviceName": "backend"
                },
                {
                    "nwbandwidth": "1.2",
                    "nwlatency": "3",
                    "podspec": {
                        "containers": [
                            {
                                "image": "quay.io/dekelly/frontend-app:v0.0.2",
                                "name": "front-end",
                                "ports": [
                                    {
                                        "containerPort": 8080,
                                        "protocol": "TCP"
                                    }
                                ]
                            }
                        ]
                    },
                    "serviceChannels": [
                        {
                            "advancedChannelSettings": {
                                "bandwidth": "5",
                                "frameSize": "100",
                                "maxDelay": "1",
                                "sendInterval": "10"
                            },
                            "channelName": "backend",
                            "otherService": {
                                "appName": "acm-swm-app",
                                "port": 8080,
                                "serviceName": "backend"
                            }
                        }
                    ],
                    "serviceName": "front-end"
                }
            ],
            "complianceClass": "High",
            "qosClass": "Gold",
            "securityClass": "Good"
        },
        "status": {
            "appMetrics": {
                "avgAppCpu": str(random.randint(1, 10)),
                "avgAppMemory": f"{random.randint(2, 6)}Gi",
                "avgAppNetworkLoad": random.randint(1, 100),
                "numPods": random.randint(1, 10),
                "serviceMetrics": [
                    {
                        "avgServiceCpu": str(avg_service_cpu),
                        "avgServiceEnergy": str(avg_node_energy),
                        "avgServiceFailure": str(avg_service_failure),
                        "avgServiceMemory": f"{random.uniform(20000000.0, 60000000.0)}", # Removed Gi
                        "clusterName":"kind-test",
                        "avgServiceSecurity": str(random.choice([0, 2])),
                        "podId": f"pod-{random.randint(1,10)}",
                        "podName": "test-pod-1",
                        "nodeName": "c1",
                        "serviceName": "front-end"
                    },
                    {
                        "avgServiceCpu": str(avg_service_cpu),
                        "avgServiceEnergy": str(avg_node_energy),
                        "avgServiceFailure": str(avg_service_failure),
                        "avgServiceMemory": f"{random.uniform(20000000.0, 60000000.0)}", # Removed Gi
                        "clusterName":"kind-test",
                        "avgServiceSecurity": f"{random.choice([0, 2])}",
                        "podId": f"pod-{random.randint(1,10)}",
                        "podName": "test-pod-2",
                        "nodeName": "c1",
                        "serviceName": "backend"
                    }
                ]
            },
            "errorMsg": "No errors",
            "nodeMetrics": [
                {
                    "avgNodeCpu": f"{avg_node_cpu}",
                    "avgNodeEnergy": f"{avg_node_energy}",
                    "avgNodeFailure": f"{avg_node_failure}",
                    "avgNodeMemory": f"{random.uniform(20000000000.0, 60000000000.0)}", # Removed Gi
                    "avgNodeSecurity": f"{random.choice([0, 2])}",
                    "nodeName": "c1"
                },
                {
                    "avgNodeCpu": str(avg_node_cpu),
                    "avgNodeEnergy": str(avg_node_energy),
                    "avgNodeFailure": str(avg_node_failure),
                    "avgNodeMemory": f"{random.uniform(20000000000.0, 60000000000.0)}", # Removed Gi
                    "avgNodeSecurity": f"{random.choice([0, 2])}",
                    "nodeName": "c2"
                }
            ],
            "status": "OK"  # You can adjust this based on your logic
        }
    }
    return spec_body

def create_or_patch_cr(mock_cr):
    spec = mock_cr.pop("spec", {})
    status = mock_cr.pop("status", {})

    cr_content = {
        "apiVersion": f"{CRD_GROUP}/{CRD_VERSION}",
        "kind": "CodecoApp",
        "metadata": {"name": CR_NAME, "namespace": CR_NAMESPACE},
        "spec": spec,
        "status": status
    }
 
    try:
        # Attempt to create the CR object
        v1.create_namespaced_custom_object(
            group=CRD_GROUP,
            version=CRD_VERSION,
            namespace=CR_NAMESPACE,
            plural=CRD_PLURAL,
            body=cr_content,
        )
        print(f"CR object '{CR_NAME}' created.")
    except client.rest.ApiException as e:
        if e.status == 409:
            # If object already exists, patch it
            v1.patch_namespaced_custom_object_status(
                group=CRD_GROUP,
                version=CRD_VERSION,
                namespace=CR_NAMESPACE,
                plural=CRD_PLURAL,
                name=CR_NAME,
                body=cr_content
            )
            print(f"CR object '{CR_NAME}' patched.")
        else:
            print(f"Error creating/patching CR object '{CR_NAME}': {e}")

# Execution
while True:
    for node in LIST_OF_NODES.split(","):
        spec_body = get_spec()
        create_or_patch_cr(spec_body)
        
        t.sleep(3)  # Wait for 3 seconds before the next iteration

# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Panagiotis Karamolegkos (UPRC) - Author

from flask import Flask, request
import random
import yaml

app = Flask(__name__)

# Define possible values for properties
properties = {
    "freshness": ["0.1", "0.3", "0.4", "n/a"],
    "compliance": ["0.8", "0.9", "1.0", "n/a"],
    "portability": ["0.5", "0.7", "1.0", "n/a"]
}

# Base YAML template
yaml_template = """
spec:
  groups: codeco.com
  versions:
  - name: v1
    served: 'true'
    storage: 'true'
    schema:
      openAPIV3Schema:
        type: object
        properties:
          spec:
            type: object
            properties:
              pod_name:
                description: identifier of the node
                type: string
                value: n/a
              freshness:
                description: healthiness of the node based on data freshness
                type: string
                value: n/a
              compliance:
                description: compliance to application requirements
                type: string
                value: n/a
              portability:
                description: portability level for a specific application
                type: string
                value: n/a
  scope: Namespaced
  names:
    plural: mdm-mons
    singular: mdm-mon
    kind: MDM
    shortNames:
    - mdm-m
"""

# Define the endpoint
@app.route('/mdm/api/v1/pdlc', methods=['GET'])
def pdlc():
    # Extract parameters from the request
    cluster = request.args.get('cluster')
    namespace = request.args.get('namespace')
    pod_name = request.args.get('pod')

    # Generate random values for CRD properties
    freshness = random.choice(properties["freshness"])
    compliance = random.choice(properties["compliance"])
    portability = random.choice(properties["portability"])

    # Parse the YAML template
    yaml_data = yaml.safe_load(yaml_template)

    # Update YAML with extracted and random values
    properties_section = yaml_data["spec"]["versions"][0]["schema"]["openAPIV3Schema"]["properties"]["spec"]["properties"]
    properties_section["pod_name"]["value"] = pod_name
    properties_section["freshness"]["value"] = freshness
    properties_section["compliance"]["value"] = compliance
    properties_section["portability"]["value"] = portability

    # Convert back to YAML format
    updated_yaml = yaml.dump(yaml_data, default_flow_style=False)

    return updated_yaml, 200, {'Content-Type': 'text/plain'}

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Panagiotis Karamolegkos (UPRC) - Author

# chmod -R 777 .
# ./delete-dummy.sh

# Delete Dummy SWM CRs
kubectl delete -f ./dummy-crds/qos-scheduler.siemens.com_assignmentplans-cr.yaml
kubectl delete -f ./dummy-crds/qos-scheduler.siemens.com_applications-cr.yaml

# Delete Dummy SWM CRDs
kubectl delete -f ./dummy-crds/qos-scheduler.siemens.com_assignmentplans.yaml
kubectl delete -f ./dummy-crds/qos-scheduler.siemens.com_applications.yaml

# Delete dummy namespaces
kubectl delete namespace test-namespace
kubectl delete namespace he-codeco-swm
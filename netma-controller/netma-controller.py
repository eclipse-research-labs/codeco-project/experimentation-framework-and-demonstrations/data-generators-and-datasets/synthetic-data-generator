# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Panagiotis Karamolegkos (UPRC) - Author

import time
from kubernetes import client, config
import yaml
import os

# Load Environment Variables
LIST_OF_NODES = os.getenv("LIST_OF_NODES", "node1,node2,node3")
LIST_OF_NODES = LIST_OF_NODES.split(",")        # Split the string into a list of node names

# Load the Kubernetes configuration from the default location
config.load_incluster_config()
v1 = client.CustomObjectsApi()

# Path to the YAML file (overlay)
file_path = "netma-crd.yaml"        # Could become an environment variable

# Open the file and load the YAML data
with open(file_path, 'r') as file:
    yaml_data = yaml.load(file, Loader=yaml.FullLoader)

# Define the CR object details
cr_namespace = yaml_data["metadata"]["namespace"]       # Namespace where the CR object should be created
cr_plural = yaml_data["spec"]["names"]["plural"]        # Plural name of the CRD (CustomResourceDefinition)
cr_group = yaml_data["spec"]["group"]                   # Group of the CRD
cr_version = yaml_data["spec"]["versions"][0]["name"]   # Version of the CRD
cr_kind = yaml_data["spec"]["names"]["kind"]            # Kind of the CRD
cr_name = f"netma-sample"                               # The name of the CR

# Function to generate synthetic values for CRs based on node names
def generate_synthetic_values_u(compute_node_names):
    synthetic_values = {
        "networkImplementation": "underlay-network",
        "physicalBase": "physical-network",
        "nodes": [],
        "links": [],
        "paths": []
    }
    # Add COMPUTE nodes
    for i, node_name in enumerate(compute_node_names):
        node_capabilities = {"uNodeDegree": i + 1, "uNodeThroughput": f"{i}G", "uNodeBandWidth": f"{i+1}G"}
        synthetic_values["nodes"].append({"name": node_name, "type": "COMPUTE", "capabilities": node_capabilities})
    
    # Add NETWORK node connecting all COMPUTE nodes
    network_node_name = "network-node"
    network_node_capabilities = {"uNodeNetFailure": 0, "uNodeDegree": len(compute_node_names)}
    synthetic_values["nodes"].append({"name": network_node_name, "type": "NETWORK", "capabilities": network_node_capabilities})
    for compute_node_name in compute_node_names:
        synthetic_values["links"].append({"name": f"link-{compute_node_name}-{network_node_name}", "source": compute_node_name, "target": network_node_name})
        synthetic_values["links"].append({"name": f"link-{network_node_name}-{compute_node_name}", "source": network_node_name, "target": compute_node_name})
    
    # Add paths between COMPUTE nodes
    for source_node in compute_node_names:
        for target_node in compute_node_names:
            if source_node != target_node:
                synthetic_values["paths"].append({
                    "name": f"path-{source_node}-{target_node}",
                    "source": source_node,
                    "target": target_node,
                    "capabilities": {
                        "uPathFailure": 0,  # Assuming no path failures for simplicity
                        "uPathLength": 1,    # Assuming path length of 1 for simplicity
                        "uPacketLoss": 0,    # Assuming no packet loss for simplicity
                        "uBandWidthBits": "1G",  # Assuming bandwidth of 1G for simplicity
                        "uLatencyNanos": "1e6"   # Assuming latency of 1e6 nanoseconds for simplicity
                    }
                })
    
    return synthetic_values

# Function to generate synthetic values for CRs based on node names
def generate_synthetic_values_o(compute_node_names):
    synthetic_values = {
        "networkImplementation": "l2sm-network",
        "physicalBase": "logical-network",
        "nodes": [],
        "links": []
    }
    # Add COMPUTE nodes
    for i, node_name in enumerate(compute_node_names):
        node_capabilities = {"oNodeDegree": i + 1, "oNodeThroughput": f"{i}G"}
        synthetic_values["nodes"].append({"name": node_name, "type": "COMPUTE", "capabilities": node_capabilities})
    
    # Add links between COMPUTE nodes
    for i, source_node in enumerate(compute_node_names):
        for j, target_node in enumerate(compute_node_names):
            if i != j:  # Avoid linking a node to itself
                node_capabilities = {"oBandWidthBits": "1185651", "oLatencyNanos": 468000}
                synthetic_values["links"].append({"name": f"link-{source_node}-{target_node}", "source": source_node, "target": target_node, "capabilities": node_capabilities})
    
    return synthetic_values

# Bind the two CR generation functions to a single function
def generate_synthetic_values(compute_node_names):
    synthetic_values = {
        "underlay-topology": generate_synthetic_values_u(compute_node_names),
        "overlay-topology": generate_synthetic_values_o(compute_node_names)
    }
    return synthetic_values

# Function to create or patch a CR
def create_or_patch_cr(compute_node_names, resource_version=None):
    # Define the CR object body with the new specifications
    cr_body = {
        "apiVersion": f"{cr_group}/{cr_version}",
        "kind": cr_kind,
        "metadata": {"name": cr_name, "namespace": cr_namespace},
        # "spec": synthetic_values,
        "underlay-topology": generate_synthetic_values_u(compute_node_names),
        "overlay-topology": generate_synthetic_values_o(compute_node_names)
    }
    
    # Try to create or patch the CR object
    try:
        v1.create_namespaced_custom_object(
            group=cr_group,
            version=cr_version,
            namespace=cr_namespace,
            plural=cr_plural,
            body=cr_body,
        )
        print(f"CR object '{cr_name}' created.")
    except client.rest.ApiException as e:
        if e.status == 409:
            # If object already exists, patch it
            v1.patch_namespaced_custom_object(
                group=cr_group,
                version=cr_version,
                namespace=cr_namespace,
                plural=cr_plural,
                name=cr_name,
                body=cr_body,
            )
            print(f"CR object '{cr_name}' patched.")
        else:
            print(f"Error creating/patching CR object '{cr_name}': {e}")

# Generate synthetic values based on COMPUTE node names
# synthetic_values = generate_synthetic_values(LIST_OF_NODES)

# Generate and create/patch CRs
while True:
    create_or_patch_cr(LIST_OF_NODES)
    time.sleep(1)  # Wait for 1 second before the next iteration
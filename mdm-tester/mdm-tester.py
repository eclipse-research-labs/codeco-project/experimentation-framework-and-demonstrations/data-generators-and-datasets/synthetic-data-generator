# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Panagiotis Karamolegkos (UPRC) - Author

import requests
import time
import os

# Get the URL as an environment variable
url = os.getenv('MDM_CONTROLLER_SERVICE_URL', "http://mdm-controller-service.he-codeco-mdm.svc.cluster.local:5000")

while True:
    response = requests.get(url+"/mdm/api/v1/pdlc?cluster=test-cluster&namespace=my-namespace&pod=test-pod")
    print(response.text)
    time.sleep(1)
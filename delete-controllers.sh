# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Panagiotis Karamolegkos (UPRC) - Author

# chmod -R 777 .
# ./delete-controllers.sh

# Delete the ACM Controller
kubectl delete -f ./acm-controller/acm-controller-deployment.yaml
kubectl delete -f ./acm-controller/acm-role-binding.yaml
kubectl delete -f ./acm-controller/acm-role.yaml
kubectl delete -f ./acm-controller/acm-crd.yaml
kubectl delete namespace he-codeco-acm

# Delete the MDM Controller
kubectl delete -f ./mdm-controller/mdm-controller-deployment.yaml
kubectl delete -f ./mdm-controller/mdm-role-binding.yaml
kubectl delete -f ./mdm-controller/mdm-role.yaml
kubectl delete -f ./mdm-controller/mdm-crd.yaml
kubectl delete namespace he-codeco-mdm

# Delete the NetMA Controller
kubectl delete -f ./netma-controller/netma-controller-deployment.yaml
kubectl delete -f ./netma-controller/netma-role-binding.yaml
kubectl delete -f ./netma-controller/netma-role.yaml
kubectl delete -f ./netma-controller/netma-crd.yaml
kubectl delete namespace he-codeco-netma
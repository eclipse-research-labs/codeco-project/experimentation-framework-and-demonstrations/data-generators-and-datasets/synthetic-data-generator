<!--
  ~ Copyright (c) 2024 University of Piraeus Research Centre
  ~ 
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~ 
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~ 
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~ 
  ~ SPDX-License-Identifier: Apache-2.0
  ~ 
  ~ Contributors:
  ~     Efterpi Paraskevoulakou (UPRC) - Co-Author
  ~     Panagiotis Karamolegkos (UPRC) - Co-Author
-->

# Prerequisites
To use this code you should have the following:
- A Kubernetes Cluster
- Be the Admin of the cluster, with access to the `default` **Service Account**.

# Description
This Code is used to extract testing values for the ACM, MDM and NetMA components of CODECO.

## How it works
During the installation three CRDs are created and three deployments with proper roles to view, list and patch the CRs of the afformationed CRDs are created. The Deployments are the controllers below:
- ACM Controller: Provides data for the behaviour of ACM.
- MDM Controller: Provides data for the behaviour of MDM.
- NetMA Controller: Provides data for the behaviour of NetMA.

The objects that are created and patched by the above controllers can be used to extract real time monitoring information of the Kubernetes Cluster.

# Installation Steps
### Adding Node Information
Before you install the controllers you must specify the Cluster's Nodes. You can use the following command to get the nodes of the cluster:
```
kubectl get nodes
```

Go in the following YAMLs and change the `spec/template/spec/containers/env/value` by adding your nodes separated by commas.

<u>YAMLs:</u>
- netma-controller/netma-controller-deployment.yaml
- acm-controller/acm-controller-deployment.yaml


As an example, below is the value for a cluster with nodes named as: `control-plane-1`, `worker-1`, `worker-2`.

<u>Example:</u>
```
env:
- name: LIST_OF_NODES
  value: "control-plane-1,worker-1,worker-2"

```

### Change the namespace of the Dummy Application (Optional)
Currently the dummy application is existing in the namespace named as `test-namespace`. You can change this by defining your own namespace name in the following files - It is not advised:
- apply-dummy.sh
- delete-dummy.sh
- dummy-crds/qos-scheduler.siemens.com_applications-cr.yaml
- acm-controller/acm-controller-deployment.yaml
- acm-controller/acm-crd.yaml

In order to make this change, you only need to refill every text string with the value `test-namespace`.

### Installing the Controllers
Run the following commands to use the installation script:
```
chmod -R 777 apply-controllers.sh
./apply-controllers.sh
```

### Installing Dummy CODECO CRDs
You can install dymmy CODECO CRDs in order to test the Data Generator with PDLC standalone. To do this, you can use the following commands to install them:
```
chmod -R 777 apply-dummy.sh
./apply-dummy.sh
```

# Uninstall Steps
Run the following commands to use the deletion script of the controllers:
```
chmod -R 777 delete-controllers.sh
./delete-controllers.sh
```

Run the following commands to use the deletion script of the Dummy CRDs and CRs:
```
chmod -R 777 delete-dummy.sh
./delete-dummy.sh
```

# Authors
- Panagiotis Karamolegkos (UPRC)
- Pepi Paraskevoulakou (UPRC)